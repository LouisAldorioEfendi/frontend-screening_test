import './App.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import React,{useState, useEffect} from 'react'
import NavBar from './components/navigationBar';
import HighLight from './components/highLight';
import Movies from './components/movies';
import Footer from './components/footer';
import axios from 'axios'

function App() {

  const [apiData, setApiData] = useState()
  const [query, setQuery] = useState(localStorage.getItem("last_query") ? localStorage.getItem("last_query") : "girls")

    useEffect(() => {
      
        axios.get(`http://api.tvmaze.com/search/shows?q=${query}`)
            .then(res => {
                setApiData(res.data)
            })

        localStorage.setItem("last_query", query)
    },[query])

    return (
      <div className="App">
        <CssBaseline />
        <Container maxWidth="lg">
          <NavBar setQuery={setQuery} query={query}/>
          <HighLight />
          <Movies apiData={apiData}/>
          <Footer />
        </Container>
      </div>
    );
}

export default App;

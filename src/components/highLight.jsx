import { Container, makeStyles } from '@material-ui/core'
import React from 'react'

const useStyles = makeStyles({
    root: {
        width: '100%',
        height: '55vh',
        backgroundImage: 'url("https://images.jdmagicbox.com/comp/jd_social/news/2018jul23/image-123331-qerkt9io8q.jpg")',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat'
    }
})

function HighLight() {

    const classes = useStyles()

    return (
        <Container maxWidth="lg" className={classes.root}>
            <span></span>
        </Container>
    )
}

export default HighLight
import { Container, Grid, makeStyles,TextField, Typography } from '@material-ui/core';
import React from 'react'

const useStyles = makeStyles({
    root: {
        position: 'fixed',
        top: 0,
        backdropFilter: 'blur(6px)'
    },
    title: {
        textAlign: 'left',
    },
    profile: {
        textAlign: 'left',
        marginTop: '3.5%'
    },
    searchBar: {
        marginTop: '2%'
    }
})

function NavBar(props) {
    const classes = useStyles()


    return (
        <Container maxWidth="lg" className={classes.root}>
            <Grid container spacing={2} >
                <Grid item lg={6} className={classes.title}>
                    <h1>Campflix</h1>
                </Grid>
                <Grid item lg={6}>
                    <Grid container spacing={2}>
                        <Grid item lg={8} className={classes.searchBar}>

                            <TextField id="filled-search" 
                                label="Movie Search" 
                                focused
                                color="primary" 
                                type="search" 
                                variant="outlined" 
                                margin="dense" 
                                value={props.query}
                                onChange={(event) => {
                                    props.setQuery(event.target.value)
                                }}
                                fullWidth/>

                        </Grid>
                        <Grid item lg={4} className={classes.profile}>
                            <Typography variant="h5" component="h6">
                                Louis Aldorio
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
        
    )
}

export default NavBar;
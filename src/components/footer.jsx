import React from 'react'
import {makeStyles} from '@material-ui/core'

const useStyles = makeStyles({
    footer: {
        width: '100%',
        height: '50px',
        textAlign: 'center',
        marginTop: 10
    }
})

function Footer(){

    const classes = useStyles()

    let currentYear = new Date().getFullYear()
    return (
        <div className={classes.footer}>Designed And Developed By Louis Aldorio &copy; {currentYear} </div>
    );
}

export default Footer;
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import Link from '@material-ui/core/Link';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import DoneIcon from '@material-ui/icons/Done';
import { Divider } from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
    root: {
        width: 345,
        overflow: "visible",
        cursor: 'pointer'
    },
    media: {
        height: 350    
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
    divider: {
        marginBottom: 10,
        marginTop: 10
    },
    genre: {
        margin: 5
    }
}));

function MovieCard(props) {

    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    return (
        <Card className={classes.root} >
            <Link href={props.data.show.url}>
                <CardMedia
                    className={classes.media}
                    image={props.data.show.image.medium}
                    title={props.data.show.name}  
                />
            </Link>

            <CardContent>
                <Typography variant="h4" color="textPrimary" component="p">
                    {props.data.show.name}           
                </Typography>
                <Typography variant="h5" color="textSecondary" component="p">
                    Score : {props.data.score}           
                </Typography>
                <Divider className={classes.divider}/>
                {
                    props.data.show.genres.map((genre,index) => (
                        <Chip
                            key={index}
                            avatar={<Avatar>{genre.charAt(0)}</Avatar>}
                            label={genre}
                            clickable
                            className={classes.genre}
                            color="primary"
                            deleteIcon={<DoneIcon />}
                        />
                    ))
                }    
                <Typography variant="h6" color="textSecondary" component="p">
                    Language : {props.data.show.language}           
                </Typography>            
            </CardContent>
            
            <CardActions disableSpacing>
                <Typography variant="p" color="textSecondary" component="p" style={{marginLeft: 10}}>
                    Expand to read the Summary         
                </Typography>
                <IconButton
                className={clsx(classes.expand, {
                    [classes.expandOpen]: expanded,
                })}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
                >
                <ExpandMoreIcon />
                </IconButton>
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    <Typography variant="body2" color="textSecondary" component="p">
                        <div dangerouslySetInnerHTML={{ __html: props.data.show.summary ? props.data.show.summary : '<p>No Summary Available</p>'}} />
                    </Typography>
                </CardContent>
            </Collapse>
        </Card>
    );
}

export default MovieCard
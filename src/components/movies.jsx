import React from 'react'
import { makeStyles } from '@material-ui/core'
import MovieCard from './movieCard'
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles({
    root: {
        marginBottom: 15,
        display: 'flex',
        overflowX: 'scroll',
        width: '100%',
        padding: '10px 0'
    },
    card: {
        margin: 10
    },
})

function Movies(props) {

    const classes = useStyles()

    if(!props.apiData) {
        return (
            <CircularProgress color="secondary"/>
        )
    }

    return (
        <div className={classes.root}>
            {
                props.apiData && props.apiData.map((item,index) => (
                    item.show.image ?
                    <div className={classes.card} key={index}>
                        <MovieCard key={index} data={item}/>
                    </div> : null
                ))
            }
        </div>

    )
}

export default Movies